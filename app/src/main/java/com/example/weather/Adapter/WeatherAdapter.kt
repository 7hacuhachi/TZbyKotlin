package com.example.weather.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.Model.WeatherModel
import com.example.weather.R
import kotlinx.android.synthetic.main.item_layout.view.*

class WeatherAdapter: RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {
    class WeatherViewHolder(view: View): RecyclerView.ViewHolder(view)

    var listWeather = emptyList<WeatherModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)
        return WeatherViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.itemView.item_title.text = listWeather[position].city
    }

    override fun getItemCount(): Int {
        return listWeather.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(list: List<WeatherModel>){
        listWeather = list
        notifyDataSetChanged()
    }
}