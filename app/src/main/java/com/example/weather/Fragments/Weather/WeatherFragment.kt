package com.example.weather.Fragments.Weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.Adapter.WeatherAdapter
import com.example.weather.Fragments.SaveWeather.SaveWeatherViewModel
import com.example.weather.Model.WeatherModel
import com.example.weather.R
import com.example.weather.ViewModel.WeatherEvent
import com.example.weather.ViewModel.WeatherViewModel
import com.example.weather.databinding.FragmentWeatherBinding

//MVVM
class WeatherFragment : Fragment() {

    private lateinit var binding: FragmentWeatherBinding
    private val viewModel by viewModels<WeatherViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGetCity.setOnClickListener {
            //MAIN.navController.navigate(R.id.action_weatherFragment_to_cityFragment)
            //val navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
            viewModel.onEvent(WeatherEvent.SelectCity(binding.editText.text.toString()))
            //Init()
            //InitTextView()
        }

        binding.btnNext.setOnClickListener {
            val navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
            navController.navigate(R.id.action_weatherFragment_to_saveWeatherFragment)
        }

    }


    override fun onStart() {
        super.onStart()
        viewModel.liveData.observe(this) {
            binding.textTemp.text = it
        }
    }
}