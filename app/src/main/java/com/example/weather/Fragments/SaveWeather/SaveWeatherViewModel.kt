package com.example.weather.Fragments.SaveWeather

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weather.Database.repository.WeatherDatabase
import com.example.weather.Database.repository.WeatherRealization
import com.example.weather.Model.WeatherModel
import com.example.weather.REPOSITORY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SaveWeatherViewModel(application: Application): AndroidViewModel(application) {
    val context = application

    fun initDatabase(){
        val daoWeather = WeatherDatabase.getInstance(context).getWeatherDao()
        REPOSITORY = WeatherRealization(daoWeather)
    }

    fun getAllWeathers(): LiveData<List<WeatherModel>> {
        return  REPOSITORY.allWeathers
    }

    fun insert(weatherModel: WeatherModel, onSuccess:() -> Unit) =
        viewModelScope.launch (Dispatchers.IO){
            REPOSITORY.insertWeather(weatherModel){
                onSuccess()
            }
        }
}