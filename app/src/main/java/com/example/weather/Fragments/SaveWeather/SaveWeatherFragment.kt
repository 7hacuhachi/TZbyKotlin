package com.example.weather.Fragments.SaveWeather

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.Adapter.WeatherAdapter
import com.example.weather.Model.WeatherModel
import com.example.weather.R
import com.example.weather.ViewModel.WeatherViewModel
import com.example.weather.databinding.FragmentSaveWeatherBinding

class SaveWeatherFragment : Fragment() {

    private lateinit var binding: FragmentSaveWeatherBinding
    private val viewModel by viewModels<SaveWeatherViewModel>()
    private lateinit var adapter: WeatherAdapter
    private lateinit var receiverView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSaveWeatherBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Init()
    }

    private fun Init() {
        val viewModelProvider = ViewModelProvider(this)
            .get(SaveWeatherViewModel::class.java)
        viewModelProvider.initDatabase()
        receiverView = binding.rvWeather
        adapter = WeatherAdapter()
        receiverView.adapter = adapter
        viewModelProvider.getAllWeathers()
            .observe(viewLifecycleOwner, {
                    listWeather ->
                adapter.setList(listWeather.asReversed())
            })
    }

    private fun InitTextView(city: String) {
        val city = city
        viewModel.insert(WeatherModel(city = city, temp = "temp")){}
    }
}