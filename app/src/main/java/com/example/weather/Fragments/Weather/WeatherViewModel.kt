package com.example.weather.ViewModel

import android.app.Application
import android.service.autofill.UserData
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import com.example.weather.API_KEY
import com.example.weather.Database.repository.WeatherDatabase
import com.example.weather.Database.repository.WeatherRealization
import com.example.weather.Fragments.SaveWeather.SaveWeatherViewModel
import com.example.weather.Model.WeatherModel
import com.example.weather.REPOSITORY
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import kotlin.concurrent.thread

sealed class WeatherEvent() {
    data class SelectCity(val city: String) : WeatherEvent()
}

data class Weather(@SerializedName("main") val main: Main){
    data class Main(val temp: String)
}

class WeatherViewModel: ViewModel() {


    private val okHttpClient: OkHttpClient = OkHttpClient();
    private val _liveData = MutableLiveData<String>()
    val liveData: LiveData<String> = _liveData

    fun onEvent(weatherEvent: WeatherEvent) {
        when (weatherEvent) {
            is WeatherEvent.SelectCity -> {
                fetchWeather(weatherEvent.city)
            }
        }
    }

    private fun fetchWeather(cityName: String) {
        val request: Request = Request.Builder().url(
            "http://api.openweathermap.org/data/2.5/weather?q=$cityName" +
                    "&units=metric&lang=ru&appid=$API_KEY"
        ).build()

        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }
            override fun onResponse(call: Call, response: Response) {
                val json = response.body()?.string()
                val gson = Gson()
                val weatherModel: Weather = gson.fromJson(json,Weather::class.java)
                _liveData.postValue(weatherModel.main.temp)
            }
        })
    }
}