package com.example.weather.Database.repository

import androidx.lifecycle.LiveData
import com.example.weather.Database.dao.IWeatherDao
import com.example.weather.Model.WeatherModel

class WeatherRealization(private val weatherDao: IWeatherDao): IWeatherRepository {

    override val allWeathers: LiveData<List<WeatherModel>>
        get() = weatherDao.getAllWeathers()

    override suspend fun insertWeather(weatherModel: WeatherModel, onSuccess: () -> Unit) {
        weatherDao.insert(weatherModel)
        onSuccess()
    }

    override suspend fun deleteWeather(weatherModel: WeatherModel, onSuccess: () -> Unit) {
        weatherDao.delete(weatherModel)
        onSuccess()
    }
}