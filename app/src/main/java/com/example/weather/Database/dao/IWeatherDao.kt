package com.example.weather.Database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weather.Model.WeatherModel

@Dao
interface IWeatherDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(weatherModel: WeatherModel)

    @Delete
    suspend fun delete(weatherModel: WeatherModel)

    @Query("SELECT * FROM weather_table")
    fun getAllWeathers():LiveData<List<WeatherModel>>
}