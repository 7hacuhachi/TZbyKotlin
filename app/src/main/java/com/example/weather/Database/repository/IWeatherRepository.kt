package com.example.weather.Database.repository

import androidx.lifecycle.LiveData
import com.example.weather.Model.WeatherModel

interface IWeatherRepository {

    val allWeathers: LiveData<List<WeatherModel>>
    suspend fun insertWeather(weatherModel: WeatherModel, onSuccess:() -> Unit)
    suspend fun deleteWeather(weatherModel: WeatherModel, onSuccess:() -> Unit)

}