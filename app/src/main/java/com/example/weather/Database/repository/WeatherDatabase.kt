package com.example.weather.Database.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.weather.Database.dao.IWeatherDao
import com.example.weather.Model.WeatherModel

@Database(entities = [WeatherModel::class], version = 2)
abstract class WeatherDatabase: RoomDatabase() {

    abstract fun getWeatherDao(): IWeatherDao

    companion object{
        private var database: WeatherDatabase ?= null

        @Synchronized
        fun getInstance(context: Context): WeatherDatabase{
            return if (database == null){
                database = Room.databaseBuilder(
                    context, WeatherDatabase::class.java, "db").build()
                database as WeatherDatabase
            }else{
                database as WeatherDatabase
            }
        }
    }
}